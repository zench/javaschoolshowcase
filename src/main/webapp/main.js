$(document).ready(function() {
    getContent();
});
function getContent() {
    var remoteServer = "http://localhost:8080";
    $.ajax({
        url: remoteServer+"/rest/showcase"
    }).then(function(data) {
        $("#showCaseTable_content").html("");
        var htmlToAdd = "";
        for(var i=0; i < data.length; i++) {
            if(i%4 === 0) {
                htmlToAdd += "<div class = 'row'>";
            }
            htmlToAdd += "<div class='col-md-3'><div class=\"mb-3 card shadow-sm text-center\">\n" +
                "    <div class=\"card-header\">"+data[i].name+"</div><div class=\"card-body\"><img height='250px' src='"+remoteServer+data[i].photoUrl+"'><br/>Цена: "+data[i].price+" &#8381;</div></div></div>";
            if(i%4 === 3) {
                htmlToAdd += "</div>";
            }
        }
        $("#showCaseTable_content").append(htmlToAdd);
    });
}