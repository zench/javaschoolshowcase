package ru.t_systems.samushenkov.showcase;

import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;

import javax.ejb.Singleton;
import javax.ejb.Stateless;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@Singleton
public class TiresBean implements MessageListener, TiresBeanInterface {

    private JmsAsyncReceiveQueueClient asyncReceiveQueueClient;

    public void onMessage(Message message) {
        TextMessage textMessage = (TextMessage) message;
        try {
            System.out.println(textMessage.getText());
            EventBus eventBus = EventBusFactory.getDefault().eventBus();
            if(textMessage.getText().equals("updateTop")) {
                eventBus.publish("/showcase", textMessage.getText());
            }
            if ("END".equals(textMessage.getText())) {
                asyncReceiveQueueClient.latchCountDown();
            }
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    public void showMessage() {
        String message = "Hello!";
        EventBus eventBus = EventBusFactory.getDefault().eventBus();
        eventBus.publish("/showcase", String.valueOf(message));
    }

    public void setAsyncReceiveQueueClientExample(
            JmsAsyncReceiveQueueClient asyncReceiveQueueClientExample) {
        this.asyncReceiveQueueClient = asyncReceiveQueueClientExample;
    }
}
