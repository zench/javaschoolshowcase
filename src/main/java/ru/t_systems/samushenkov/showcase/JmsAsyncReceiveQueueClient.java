package ru.t_systems.samushenkov.showcase;

import org.apache.activemq.ActiveMQConnectionFactory;

import java.util.concurrent.CountDownLatch;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;

@Stateless
public class JmsAsyncReceiveQueueClient {

    private CountDownLatch latch = new CountDownLatch(1);

    public void receiveMessages() throws JMSException, InterruptedException {
        Connection connection = null;
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
                "tcp://localhost:61616");
        connection = connectionFactory.createConnection();
        Session session = connection.createSession(false,
                Session.AUTO_ACKNOWLEDGE);
        try {
            Queue queue = session.createQueue("tires");

            // Consumer
            MessageConsumer consumer = session.createConsumer(queue);
            TiresBean consumerListener = new TiresBean();
            consumer.setMessageListener(consumerListener);
            consumerListener.setAsyncReceiveQueueClientExample(this);

            connection.start();
            latch.await();
        } finally {
            if (session != null) {
                session.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }

    public void latchCountDown() {
        latch.countDown();
    }
}
