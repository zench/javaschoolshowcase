package ru.t_systems.samushenkov.showcase;

import javax.ejb.EJB;
import javax.jms.JMSException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/test")
public class App extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JmsAsyncReceiveQueueClient asyncReceiveClient = new JmsAsyncReceiveQueueClient();
        try {
            asyncReceiveClient.receiveMessages();
        } catch (JMSException ignored) {

        } catch (InterruptedException ignored) {

        }
        resp.getWriter().println("Hello from servlet");
    }
}