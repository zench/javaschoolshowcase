package ru.t_systems.samushenkov.showcase;

import javax.ejb.Local;

@Local
public interface TiresBeanInterface {
    void showMessage();
}
