package ru.t_systems.samushenkov.showcase;

import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import java.io.Serializable;

@ManagedBean
@ApplicationScoped
public class GlobalMessengerBean implements Serializable {
    private String tiresShopUrl = "http://localhost:8080";
    private String message;

    @EJB
    TiresBeanInterface tiresBean;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTiresShopUrl() {
        return tiresShopUrl;
    }

    public void setTiresShopUrl(String tiresShopUrl) {
        this.tiresShopUrl = tiresShopUrl;
    }

    public synchronized void showMessage() {
        this.message = "Hello!";
        tiresBean.showMessage();
    }
}
